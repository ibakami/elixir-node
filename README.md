ibakami/elixir-node
===

To build from scratch

```
git clone https://gitlab.com/ibakami/elixir-node.git
cd elixir-node
docker build -t ibakami/elixir-node .
```

To use as a task in a `.gitlab-ci.yml`:

```
test:
  image: ibakami/elixir-node
```

To use as a base for Elixir + Phoenix builds:

```
FROM ibakami/elixir-node
```
