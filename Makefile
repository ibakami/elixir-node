.PHONY: help

ELIXIR_VERSION ?= '1.9.1'

help:
	@echo "$(APP_NAME):$(APP_VSN)-$(BUILD)"
	@perl -nle'print $& if m{^[a-zA-Z_-]+:.*?## .*$$}' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build: ## Build the Docker images
	docker build \
	--build-arg BASE_TAG=${ELIXIR_VERSION} \
	-t ibakami/elixir-node:${ELIXIR_VERSION} \
	-t ibakami/elixir-node:latest .
	docker build \
	--build-arg BASE_TAG=${ELIXIR_VERSION}-alpine \
	-f alpine.Dockerfile \
	-t ibakami/elixir-node:${ELIXIR_VERSION}-alpine .

run: ## Run the app in Docker
	docker run --env-file config/docker.env \
	--expose 4000 -p 4000:4000 \
	--rm -it $(APP_NAME):latest
