ARG BASE_TAG=1.9.1
FROM elixir:${BASE_TAG}

RUN apk add --no-cache nodejs yarn git build-base &&\
    mix local.rebar --force &&\
    mix local.hex --force
